module.exports = function(options) {
  return function zrada(req, res, next) {
    if (req.body && req.body.name && req.body.name.indexOf('зрада') !== -1){
		req.app.io.emit('zrada')
	    res.status(400).send('zrada');
    } else  {
	    next();
    }

  }
};