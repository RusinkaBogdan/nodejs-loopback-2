'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
const path = require('path');
var app = module.exports = loopback();

app.set('views', path.join(__dirname, '/../views'));
app.set('view engine', 'pug');


app.use(loopback.static(path.join(__dirname, '/../public/')));

var socketIO = require('socket.io');


app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.io = require('socket.io')(app.start(), { 
            path: '/socket.io',
            'transports': [
                'polling',
                'websocket'
            ],
            // forceNew: true
            "cookie": false
        });
    app.io.on('connection', function(socket){
      console.log('a user connected');
       
      socket.on('subscribe', function(room) { 
          console.log('joining room', room);
          socket.join(room); 
      });
      socket.on('disconnect', function(){
        console.log('user disconnected');
      });
    });
});
