var $$addTask = document.getElementById('add-task');
var $$tasksContainer = document.getElementById('tasks-container');
const socket = io.connect('http://localhost:3000');

fetch('/api/task').then(function(response){
	if(response.ok) {
		return response.json();
	} 
}).then(function(tasks){
	renderTasks(tasks);
	bindEventListeners();
});

function renderTasks(tasks){
	for (var i = 0; i < tasks.length; i++){
		$$tasksContainer.appendChild(renderTask(tasks[i]));
	}
}

function renderTask(task = {}){
	var $taskContainer = document.createElement('div');
	$taskContainer.className = 'task-container';

	if (task.id){
		$taskContainer.id = task.id

		var $taskId = document.createElement('a');
		$taskId.innerText = $$tasksContainer.childNodes.length + 1;
		$taskId.href = '/api/task/' + task.id;
		$taskContainer.appendChild($taskId);
	}

	var $taskName = document.createElement('input');
	$taskName.value = task.name || '';
	$taskName.className = 'task-name';
	$taskName.addEventListener('blur', blur);
	$taskContainer.appendChild($taskName);

	var $deleteTaskButton = document.createElement('button');
	$deleteTaskButton.innerText = 'Delete';
	$deleteTaskButton.className = 'delete-task'
	$taskContainer.appendChild($deleteTaskButton);
	return $taskContainer;
}

function createAlert(){
	var $alert = document.createElement('div');
	$alert.id = 'zrada';
	$alert.innerText = 'Спроба зради!';
	return $alert;
}

function bindEventListeners(){

	socket.on('task-added', (item)=>{
		console.log('wtf')
		$$tasksContainer.appendChild(renderTask(item));
		$$addTask.value = '';
	});

	socket.on('task-deleted', (item)=>{
		var task = document.getElementById(item.id);
		$$tasksContainer.removeChild(task);
	});

	socket.on('task-edited', (item)=>{
		var task = document.getElementById(item.id);
		task.querySelector('.task-name').value = item.name;	
	});

	socket.on('zrada', (item)=>{
		console.log('zrada!!')
		var alertMessage = createAlert();
		document.body.appendChild(alertMessage);
		setTimeout(function(){
			document.body.removeChild(alertMessage);
		}, 2000);
	});

	$$addTask.addEventListener('keypress', function(e){
		if (e.charCode === 13){
			sendCreateTaskReq(e.target.value);
		}
	});

	var items = $$tasksContainer.querySelectorAll('.task-name');
	items.forEach((item)=>{
		item.addEventListener('blur', blur);
		item.addEventListener('keypress', editByKeyPress.bind(this, item));

	});

	document.addEventListener('click', clickHandler);

}

function clickHandler(event){

	 if (event.target.className === 'delete-task'){
		var taskContainer = event.target.parentNode;
		var id = taskContainer.id;
		if (id) {
			sendDeleteTaskReq(id);
		}
	}
}

function editByKeyPress(item, e){
	if (e.charCode === 13){
		sendEditTaskReq(item.parentNode, item.parentNode.id);
		item.blur();
	}
}

function blur(event){
	if (event.target.id !== 'add-task'){
		sendEditTaskReq(event.target.parentNode, event.target.parentNode.id);
	}
}

function sendEditTaskReq(taskContainer, id){
	console.log(id)
	var name = taskContainer.querySelector('.task-name').value;

	return fetch('/api/task/' + id, {
		method: 'PUT',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			name: name
		})
	})
}

function sendDeleteTaskReq(id){
	return fetch('/api/task/' + id, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json'
		}
	})
}

function sendCreateTaskReq(name){
	return fetch('/api/task/', {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			name: name
		})
	})
}