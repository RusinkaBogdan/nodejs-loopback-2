

module.exports = function(Task) {
		Task.observe('after save', function(ctx, next) {
			console.log('created', ctx.instance)
			if (ctx.isNewInstance){
				Task.app.io.emit('task-added', ctx.instance);
			} else {
				console.log('updates', ctx.instance)
				Task.app.io.emit('task-edited', ctx.instance);
			}
		});

		Task.observe('after delete', function(ctx, next) {
			
			Task.app.io.emit('task-deleted', ctx.where);
		});

}